#!/usr/bin/env bash
################################################
#
# Backup into Box.com script
#
# @author   Silotrix.com
# @email    info [at] silotrix [dot] com
# @reldate  2014-08-31
################################################
#  Copyright (c) 2014 silotrix.com

usage()
{
cat << EOF
Usage: $0 [-u someone@example.com] [-p 11629147f0b43724bac73c226fad7164001d0] [-n example.com] [-r 11111111] [-a example-name] [-v]

This script update your DNS settings through API

OPTIONS:
-h      Show this message
-u      API email that specifies at runtime
-p      API key that specifies at runtime
-r      DNS record ID to update
-a      DNS record name to update
-n      Domain name
-v      Display debug infomation

EOF
}

set -e

CLOUDFLARE_API_URL="https://www.cloudflare.com/api_json.html"
CLOUDFLARE_API_EMAIL="someone@example.com"
CLOUDFLARE_API_KEY="sample-api-key"
CLOUDFLARE_API_REC_ID="11111111"
CLOUDFLARE_API_REC_NAME="example-name"
CLOUDFLARE_API_DOMAIN="example.com"

DEBUG_OUTPUT=0

while getopts "hu:p:r:a:n:v" OPTION
do
case $OPTION in
    h)
        usage
        exit 1
    ;;
    u)
        CLOUDFLARE_API_EMAIL=$OPTARG
    ;;
    p)
        CLOUDFLARE_API_KEY=$OPTARG
    ;;
    r)
        CLOUDFLARE_API_REC_ID=$OPTARG
    ;;
    a)
        CLOUDFLARE_API_REC_NAME=$OPTARG
    ;;
    n)
        CLOUDFLARE_API_DOMAIN=$OPTARG
    ;;
    v)
        DEBUG_OUTPUT=1
    ;;
    ?)
        usage
        exit
    ;;
esac
done

if [ 1 -eq $DEBUG_OUTPUT ]; then
  curl https://www.cloudflare.com/api_json.html \
  -d 'a=rec_load_all' \
  -d email=$CLOUDFLARE_API_EMAIL \
  -d tkn=$CLOUDFLARE_API_KEY \
  -d z=$CLOUDFLARE_API_DOMAIN
  echo 
  echo CLOUDFLARE_API_EMAIL: $CLOUDFLARE_API_EMAIL
  echo CLOUDFLARE_API_KEY: $CLOUDFLARE_API_KEY
  echo CLOUDFLARE_API_REC_ID: $CLOUDFLARE_API_REC_ID
  echo CLOUDFLARE_API_REC_NAME: $CLOUDFLARE_API_REC_NAME
  echo CLOUDFLARE_API_DOMAIN: $CLOUDFLARE_API_DOMAIN
fi

CLOUDFLARE_API_EXT_IP=`curl http://ipecho.net/plain 2>/dev/null`
if [ 1 -eq $DEBUG_OUTPUT ]; then
  echo External IP: $CLOUDFLARE_API_EXT_IP
fi

curl $CLOUDFLARE_API_URL \
  -d email=$CLOUDFLARE_API_EMAIL \
  -d tkn=$CLOUDFLARE_API_KEY \
  -d a=rec_edit \
  -d z=$CLOUDFLARE_API_DOMAIN \
  -d id=$CLOUDFLARE_API_REC_ID \
  -d name=$CLOUDFLARE_API_REC_NAME \
  -d type=A \
  -d service_mode=0 \
  -d content=$CLOUDFLARE_API_EXT_IP \
  -d ttl=300 2>/dev/null >/dev/null

if [ 1 -eq $DEBUG_OUTPUT ]; then
  echo DONE.
fi
