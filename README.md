# Usage

Change corresponding configuration directly from the script

```
CLOUDFLARE_API_URL="https://www.cloudflare.com/api_json.html"
CLOUDFLARE_API_EMAIL="someone@example.com"
CLOUDFLARE_API_KEY="sample-api-key"
CLOUDFLARE_API_REC_ID="11111111"
CLOUDFLARE_API_REC_NAME="example-name"
CLOUDFLARE_API_DOMAIN="example.com"
```

Or run with 

```
Usage: ./dns-update.sh [-u someone@example.com] [-p 11629147f0b43724bac73c226fad7164001d0] [-n example.com] [-r 11111111] [-a example-name] [-v]
```

Parameters:

```
-h      Show this message
-u      API email that specifies at runtime
-p      API key that specifies at runtime
-r      DNS record ID to update
-a      DNS record name to update
-n      Domain name
-v      Display debug infomation
```

# License

This software is available under the following licenses:

* MIT
* Apache 2